class Car < ActiveRecord::Base
    self.table_name = 'car'


    belongs_to :customer, :class_name => 'Customer', :foreign_key => :owner_id
    has_many :images, :class_name => 'Image'
end
