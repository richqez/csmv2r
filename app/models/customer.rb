class Customer < ActiveRecord::Base
    self.table_name = 'customer'


    has_many :cars, :class_name => 'Car'
end
