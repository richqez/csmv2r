class AuthController < ApplicationController


  def new
    render :layout => false
  end

  def create
    user = User.find_by(username: params[:auth][:username].downcase)
    if user && user.authenticate(params[:auth][:password])
      session[:user_id] = user.id
      redirect_to '/customers'
      else
      flash.now[:danger] = 'Invalid email/password combination'
      render 'new'
    end
  end

  def destroy

  end


end
