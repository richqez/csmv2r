class CustomersController < ApplicationController



  before_filter :authorize

  def index
    @customers = Customer.all
  end

  def dt
    @resultSearch = Customer
                        .where('name like ?','%'+params[:sSearch]+'%')
                        .limit(params[:iDisplayLength])
                        .offset(params[:iDisplayStart])

    @resultCountAll = Customer.count

    @dt = {'aaData' => @resultSearch ,
           'iTotalRecords'=> @resultSearch.size,
           'iTotalDisplayRecords' => @resultCountAll,
           'sEcho'=>params[:sEcho]
    }
    render json: @dt
  end

end
