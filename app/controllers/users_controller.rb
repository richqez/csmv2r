class UsersController < ApplicationController

  def new
    @user =User.new
    render :layout => false
  end

  def create
    @user = User.new(user_params)
    if @user.save
      flash.now[:success] = 'create success'
      render 'new'
    else
      flash.now[:success] = 'create error'
      render 'new'
    end
  end

  private
    def user_params
      params.require(:user).permit(:username,:password)
    end

end
