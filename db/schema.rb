# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151007125639) do

  create_table "car", force: :cascade do |t|
    t.integer  "owner_id",         limit: 4,   null: false
    t.string   "type_code",        limit: 20,  null: false
    t.string   "register_number",  limit: 20,  null: false
    t.date     "register_date",                null: false
    t.date     "texdue",                       null: false
    t.string   "insurance_detail", limit: 255, null: false
    t.string   "chassis",          limit: 255, null: false
    t.string   "remark",           limit: 255
    t.datetime "create_date"
  end

  add_index "car", ["owner_id"], name: "owner_id", using: :btree

  create_table "customer", force: :cascade do |t|
    t.string "name",     limit: 50, null: false
    t.string "lastname", limit: 50, null: false
    t.string "tel1",     limit: 10, null: false
    t.string "tel2",     limit: 10
  end

  create_table "images", force: :cascade do |t|
    t.integer "car_id",      limit: 4,          null: false
    t.binary  "data",        limit: 4294967295, null: false
    t.date    "create_date",                    null: false
  end

  add_index "images", ["car_id"], name: "car_id", using: :btree

  create_table "user", force: :cascade do |t|
    t.string "username",        limit: 255, null: false
    t.string "password",        limit: 255, null: false
    t.string "password_digest", limit: 255
  end

  add_foreign_key "car", "customer", column: "owner_id", name: "car_ibfk_1", on_update: :cascade, on_delete: :cascade
  add_foreign_key "images", "car", name: "images_ibfk_1", on_update: :cascade, on_delete: :cascade
end
